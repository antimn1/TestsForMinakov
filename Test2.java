package com.fico.dms.hub.rest.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fico.dms.hub.client.cxf.HubClientBuilder;
import com.fico.dms.hub.rest.api.ApiV1Root;
import com.fico.dms.hub.rest.api.EventsEndpoint;
import com.fico.dms.hub.rest.message.ContentMessage;
import com.fico.dms.hub.rest.model.Event;

/**
 * @author SergeiBlockiy@fico.com
 *
 */
public class EventsApiITest {
	

	private static final Logger LOG = LoggerFactory.getLogger(EventsApiITest.class);

	public EventsApiITest() {

	}
	
	protected String getBaseUrl() {
		return System.getenv().get("dmshub.base.url");
	}
	
	protected ApiV1Root getApi() {
		return (new HubClientBuilder()).withBaseURL( getBaseUrl() ).build();
	}
	
	@Test
	public void testEventPublish() {
		EventsEndpoint eventsApi = getApi().getEventsEndpoint();
		
		Event event = new Event();
		
		event.setKind("event");
		event.setType("test");
		event.setSource(this.getClass().getSimpleName());
		event.setDescription("Simple event for integration test: testEventCreation");
		
		ContentMessage<Event> createdEvent = eventsApi.create(event);
		Assert.assertNotNull(createdEvent);
		Assert.assertNotNull(createdEvent.getContent());
		Assert.assertNotNull(createdEvent.getContent().getId());
		LOG.info("testEventPublish --- PASSED");
	}
	
	@Test
	public void testProfileActivation() {
		LOG.info("testProfileActivation --- PASSED");
	}
}
