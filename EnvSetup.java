
package com.fico.dms.hub.rest;

import org.testng.annotations.BeforeSuite;

/**
 * @author SergeiBlockiy@fico.com
 *
 */
public class EnvSetup {

	
	public EnvSetup() {
	}

	@BeforeSuite
	public void setupEnv() {
		
	}
	
	protected void setGitLabAdminPassword() {
		@SuppressWarnings("unused")
		final String password = getGitLabPassword();
	}

	protected String getDmsHubBaseUrl() {
		return System.getenv().get("dmshub.base.url");
	}

	protected String getGitLabBaseUrl() {
		return System.getenv().get("dmshub.gitlab.url");
	}

	protected String getGitLabUsername() {
		return System.getenv().get("dmshub.gitlab.username");
	}

	protected String getGitLabPassword() {
		return System.getenv().get("dmshub.gitlab.password");
	}

	public void cleanEnv() {
		
	}
	
	
	
}
