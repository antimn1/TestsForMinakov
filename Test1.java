package com.fico.dms.hub.rest.projects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fico.dms.hub.client.cxf.HubClientBuilder;
import com.fico.dms.hub.rest.api.ApiV1Root;
import com.fico.dms.hub.rest.api.ProjectsEndpoint;
import com.fico.dms.hub.rest.message.ContentMessage;
import com.fico.dms.hub.rest.message.PagedContentMessage;
import com.fico.dms.hub.rest.model.Project;
import com.fico.dms.hub.rest.model.filter.ProjectsFilter;

/**
 * @author SergeiBlockiy@fico.com
 *
 */
public class ProjectsApiITest {

	
	private static final Logger LOG = LoggerFactory.getLogger(ProjectsApiITest.class);

	public ProjectsApiITest() {
	}
	
	protected String getBaseUrl() {
		return System.getenv().get("dmshub.base.url");
	}
	
	protected ApiV1Root getApi() {
		return (new HubClientBuilder()).withBaseURL( getBaseUrl() ).build();
	}
	
	@Test
	public void testNewProject() {
		ProjectsEndpoint projectsApi = getApi().getProjectsEndpoint();
		
		final String projectId = "testNewProjectId";
		Project project = new Project();
		project.setId(projectId);
		project.setName(projectId);
		project.setPublic(true);
		
		ContentMessage<Project> createdProjectMsg = projectsApi.createProject(project);
		
		Assert.assertNotNull(createdProjectMsg);
		Assert.assertNotNull(createdProjectMsg.getContent());
		Assert.assertNotNull(createdProjectMsg.getContent().getId());
		Assert.assertEquals(createdProjectMsg.getContent().getId(), projectId);

		Assert.assertNotNull(createdProjectMsg.getContent().getStorages());
		Assert.assertNotNull(createdProjectMsg.getContent().getVersion());
		Assert.assertNotNull(createdProjectMsg.getContent().getCreatedOn());
		
		LOG.info("testProjectCreate --- PASSED");
	}
	
	@Test
	public void testProjectSearch() {
		ProjectsEndpoint projectsApi = getApi().getProjectsEndpoint();
		
		ProjectsFilter filter = new ProjectsFilter();
		PagedContentMessage<Project> result = projectsApi.findPublicProjects(filter);
		
		Assert.assertNotNull(result);
		Assert.assertNotEquals(result.getIsError(), Boolean.TRUE);
	}
}
